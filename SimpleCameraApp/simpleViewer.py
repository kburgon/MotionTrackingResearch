# Simple Camera Viewer

import numpy as np
import cv2

cap = cv2.VideoCapture(0)
keepCapturing = True

while(keepCapturing):
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow('frame', gray)
    if cv2.waitKey(0) & 0xFF == ord('q'):
        keepCapturing = False

cap.release()
cv2.destroyAllWindows()